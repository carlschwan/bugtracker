# Bugtracker software

For the moment only read only

## Development

Need recent version of PHP > 7.4

```
cp .env .env.local
vim .env.local # configure database
composer install
bin/console doctrine:database:create && bin/console doctrine:schema:update --force && bin/console app:import:bugzilla
bin/console server:run
```
