<?php

namespace App\Controller;

use App\Entity\Bug;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BugController extends AbstractController
{
    /**
     * @Route("/bug/{displayId}", name="app_bug")
     * @param Bug $bug
     * @return Response
     */
    public function show(Bug $bug): Response
    {
        return $this->render('bug/index.html.twig', [
            'bug' => $bug
        ]);
    }
}
