<?php

namespace App\Controller;

use App\Entity\Component;
use App\Entity\Product;
use App\Repository\ComponentRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * @Route("/products", name="app_products")
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function index(EntityManagerInterface $em): Response
    {
        /** @var Product[] $products */
        $products = $em->getRepository(Product::class)->findAll();
        return $this->render('product/index.html.twig', [
            'products' => $products,
        ]);
    }

    /**
     * @Route("/product/{name}", name="app_product")
     * @param Product $product
     * @return Response
     */
    public function show(Product $product): Response
    {
        return $this->render('product/show.html.twig', [
           'product' => $product,
        ]);
    }

    /**
     * @Route("/product/{nameProduct}/component/{nameComponent}", name="app_component")
     * @param string $nameProduct
     * @param string $nameComponent
     * @param EntityManagerInterface $em
     * @return Response
     * @throws NonUniqueResultException
     */
    public function showComponent(string $nameProduct, string $nameComponent, EntityManagerInterface $em): Response
    {
        /** @var ComponentRepository $componentRepository */
        $componentRepository = $em->getRepository(Component::class);
        //$component = $componentRepository->findByProductNameAndComponentName($nameProduct, $nameComponent);
        $query = $em->createQuery("SELECT p, c FROM App\Entity\Product p JOIN p.components c WHERE p.name = :nameProduct AND c.name = :nameComponent");
        $query->setParameters([
            'nameProduct' => $nameProduct,
            'nameComponent' => $nameComponent,
        ]);
        /** @var Product $product */
        $product = $query->getOneOrNullResult();

        return $this->render('product/component.html.twig', [
            'component' => $product->getComponents()[0],
        ]);
    }
}
