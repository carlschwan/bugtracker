<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\BugStatus;
use App\Entity\Severity;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BugRepository")
 */
class Bug
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Severity")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Severity $severity;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BugStatus")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?BugStatus $status;

    /**
     * @ORM\Column(type="datetime")
     */
    private ?\DateTimeInterface $creation_time;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Priority")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Priority $priority;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="component")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Product $product;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Component", inversedBy="bugs")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Component $component;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private ?string $title;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $displayId;

    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="bug", orphanRemoval=true)
     */
    private ?Collection $comments;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="bugs")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?User $creator;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Bug", inversedBy="blocks")
     */
    private ?Collection $depends;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Bug", mappedBy="depends")
     */
    private ?Collection $blocks;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $operatingSystem;

    /**
     * @ORM\Column(type="string", length=512, nullable=true)
     */
    private ?string $resolvedBy;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $resolvedIn;

    public function __construct()
    {
        $this->comments = new ArrayCollection();
        $this->depends = new ArrayCollection();
        $this->blocks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSeverity(): ?Severity
    {
        return $this->severity;
    }

    public function setSeverity(?Severity $severity): self
    {
        $this->severity = $severity;

        return $this;
    }

    public function getStatus(): ?BugStatus
    {
        return $this->status;
    }

    public function setStatus(?BugStatus $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreationTime(): ?\DateTimeInterface
    {
        return $this->creation_time;
    }

    public function setCreationTime(\DateTimeInterface $creation_time): self
    {
        $this->creation_time = $creation_time;

        return $this;
    }

    public function getPriority(): ?Priority
    {
        return $this->priority;
    }

    public function setPriority(?Priority $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getComponent(): ?Component
    {
        return $this->component;
    }

    public function setComponent(?Component $component): self
    {
        $this->component = $component;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDisplayId(): ?int
    {
        return $this->displayId;
    }

    public function setDisplayId(int $displayId): self
    {
        $this->displayId = $displayId;

        return $this;
    }

    /**
     * @return Collection|Comment[]
     */
    public function getComments(): Collection
    {
        return $this->comments;
    }

    public function addComment(Comment $comment): self
    {
        if (!$this->comments->contains($comment)) {
            $this->comments[] = $comment;
            $comment->setBug($this);
        }

        return $this;
    }

    public function removeComment(Comment $comment): self
    {
        if ($this->comments->contains($comment)) {
            $this->comments->removeElement($comment);
            // set the owning side to null (unless already changed)
            if ($comment->getBug() === $this) {
                $comment->setBug(null);
            }
        }

        return $this;
    }

    public function getCreator(): ?User
    {
        return $this->creator;
    }

    public function setCreator(?User $creator): self
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getDepends(): Collection
    {
        return $this->depends;
    }

    public function addDepend(self $depend): self
    {
        if (!$this->depends->contains($depend)) {
            $this->depends[] = $depend;
        }

        return $this;
    }

    public function removeDepend(self $depend): self
    {
        if ($this->depends->contains($depend)) {
            $this->depends->removeElement($depend);
        }

        return $this;
    }

    /**
     * @return Collection|self[]
     */
    public function getBlocks(): Collection
    {
        return $this->blocks;
    }

    public function addBlock(self $block): self
    {
        if (!$this->blocks->contains($block)) {
            $this->blocks[] = $block;
            $block->addDepend($this);
        }

        return $this;
    }

    public function removeBlock(self $block): self
    {
        if ($this->blocks->contains($block)) {
            $this->blocks->removeElement($block);
            $block->removeDepend($this);
        }

        return $this;
    }

    public function getOperatingSystem(): ?string
    {
        return $this->operatingSystem;
    }

    public function setOperatingSystem(?string $operatingSystem): self
    {
        $this->operatingSystem = $operatingSystem;

        return $this;
    }

    public function getResolvedBy(): ?string
    {
        return $this->resolvedBy;
    }

    public function setResolvedBy(?string $resolvedBy): self
    {
        $this->resolvedBy = $resolvedBy;

        return $this;
    }

    public function getResolvedIn(): ?string
    {
        return $this->resolvedIn;
    }

    public function setResolvedIn(?string $resolvedIn): self
    {
        $this->resolvedIn = $resolvedIn;

        return $this;
    }
}
