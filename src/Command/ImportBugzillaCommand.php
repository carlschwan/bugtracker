<?php

namespace App\Command;

use App\Entity\Bug;
use App\Entity\BugStatus;
use App\Entity\Comment;
use App\Entity\Component;
use App\Entity\Priority;
use App\Entity\Product;
use App\Entity\Severity;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class ImportBugzillaCommand extends Command
{
    protected static $defaultName = 'app:import:bugzilla';

    private EntityManagerInterface $em;
    private UserPasswordEncoderInterface $passwordEncoder;

    public function __construct(string $name = null, EntityManagerInterface $em, UserPasswordEncoderInterface $passwordEncoder)
    {
        parent::__construct($name);
        $this->em = $em;
        $this->passwordEncoder = $passwordEncoder;
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('arg1');
        $instance = 'https://bugs.kde.org/rest/';

        $client = new \GuzzleHttp\Client();

        $response = $client->request('GET', $instance . 'product?type=accessible');
        $response = json_decode($response->getBody());

        $p = $this->em->getRepository(Priority::class)->findAll();
        $priorities = [];
        foreach ($p as $priority) {
            $priorities[$priority->getValue()] = $priority;
        }
        $s = $this->em->getRepository(Severity::class)->findAll();
        $severities = [];
        foreach ($s as $severity) {
            $severities[$severity->getValue()] = $severity;
        }
        $st = $this->em->getRepository(BugStatus::class)->findAll();
        $statusList = [];
        foreach ($st as $status) {
            $statusList[$st->getValue()] = $status;
        }

        $i = 0;
        $is = count($response->products);
        foreach ($response->products as $productObj) {
            $product = new Product();
            $product->setName($productObj->name);
            $product->setDescription($productObj->description);
            $product->setActive(true);

            $this->em->persist($product);

            foreach ($productObj->components as $componentObj) {
                $component = new Component();
                $component->setName($componentObj->name);
                $component->setDescription($componentObj->description);
                $product->addComponent($component);
                $this->em->persist($component);

                $bugs = json_decode($client->request('GET', $instance . 'bug?product=' . $product->getName() . '&component=' . $component->getName())->getBody());

                foreach ($bugs->bugs as $bugObj) {
                    $bug = new Bug();
                    $bug->setDisplayId($bugObj->id);
                    $bug->setCreationTime(new \DateTime($bugObj->creation_time));
                    $bug->setComponent($component);
                    $bug->setProduct($product);
                    $bug->setTitle($bugObj->summary);
                    /** @var ?Priority $priority */
                    $priority = null;
                    if (!array_key_exists($bugObj->priority, $priorities)) {
                        $priority = new Priority();
                        $priority->setActive(true);
                        $priority->setSortKey(0);
                        $priority->setValue($bugObj->priority);
                        $this->em->persist($priority);
                    } else {
                        $priority = $priorities[$bugObj->priority];
                    }
                    $bug->setPriority($priority);

                    /** @var ?Severity $severity */
                    $severity = null;
                    if (!array_key_exists($bugObj->severity, $severities)) {
                        $severity = new Severity();
                        $severity->setActive(true);
                        $severity->setSortKey(0);
                        $severity->setValue($bugObj->severity);
                        $this->em->persist($severity);
                    } else {
                        $severity = $severities[$bugObj->severity];
                    }
                    $bug->setSeverity($severity);

                    /** @var ?BugStatus $status */
                    $status = null;
                    if (!array_key_exists($bugObj->status, $statusList)) {
                        $status = new BugStatus();
                        $status->setActive(true);
                        $status->setSortKey(0);
                        $status->setValue($bugObj->status);
                        $this->em->persist($status);
                    } else {
                        $status = $statusList[$bugObj->status];
                    }
                    $bug->setCreator($this->getOrCreateCreatorFromObj($bugObj->creator_detail));
                    $bug->setResolvedBy($bugObj->cf_commitlink !== "" ? $bugObj->cf_commitlink : null);
                    $bug->setResolvedIn($bugObj->cf_versionfixedin !== "" ? $bugObj->cf_versionfixedin : null);

                    $comments = json_decode($client->request('GET', $instance . 'bug/' . $bug->getDisplayId() . '/comment')->getBody(), true);
                    foreach ($comments["bugs"][$bug->getDisplayId()]["comments"] as $commentObj) {
                        $comment = new Comment();
                        $comment->setBug($bug);
                        $comment->setCreator($bug->getCreator());
                        $comment->setText($commentObj["text"]);
                        $comment->setTime(new \DateTime($commentObj["time"]));
                        $this->em->persist($comment);
                    }
                    $bug->setStatus($status);
                    $this->em->persist($bug);
                }
                $i++;
                echo "$i / $is\n";
                $this->em->flush();
                if ($i > 3) { // used for testing to not ddos bugs.kde.org
                    return 0;
                }
            }

            $this->em->persist($product);
        }
        $this->em->flush();

        return 0;
    }

    public function getOrCreateCreatorFromObj($creatorObj): User {
        $userRepository = $this->em->getRepository(User::class);
        $user = $userRepository->findOneBy(["email" => $creatorObj->email]);

        if (!$user) {
            $user = new User();
            $user->setEmail($creatorObj->email);
            $user->setFullName($creatorObj->real_name);
            $user->setPassword($this->passwordEncoder->encodePassword($user, random_bytes(40)));
            $this->em->persist($user);
            $this->em->flush();
        }
        return $user;
    }
}
