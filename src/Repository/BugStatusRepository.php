<?php

namespace App\Repository;

use App\Entity\BugStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BugStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method BugStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method BugStatus[]    findAll()
 * @method BugStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BugStatusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BugStatus::class);
    }

    // /**
    //  * @return BugStatus[] Returns an array of BugStatus objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BugStatus
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
